const fs = require('fs');
const crypto = require('crypto');
const util = require('util');
const Repository = require('./repository');

const scrypt = util.promisify(crypto.scrypt);

class UsersRepository extends Repository {
    async comparePasswords(saved, supplied){
        //Saved -> password saved in our database. 'hashed.salt'
        //Supplied -> password given to us by a user trying sign in 
        // const result = saved.split('.');
        // const hashed = result[0];
        // const salt = result[1];
        const [hashed, salt] = saved.split('.');
        const hashedSuppliedBuf = await scrypt(supplied, salt, 64);
 
        return hashed === hashedSuppliedBuf.toString('hex');
 
    }

    async create(attrs){
        //attrs === { email: '' , password: ''}
        attrs.id = this.randomId();
 
        const salt = crypto.randomBytes(8).toString('hex');
        const buf = await scrypt(attrs.password, salt, 64);
        
        const records = await this.getAll();
        const record = {
            ...attrs,
            password: `${buf.toString('hex')}.${salt}`
        };
        records.push(record);
 
        await this.writeAll(records);
 
        return record;
        
    }
}

module.exports = new UsersRepository('users.json');

// //Another file...
// const repo = require('./users');
// repo.getAll();
// get.getOne();


// //yet another file:
// const repo = require('./users');
// repo.getAll();



// const test = async() => {
//     const repo = new UsersRepository('users.json');

//     const user = await repo.getOneBy({ email: 'test@test.com' });
//     console.log(user);

//     // await repo.update('123466', { password: 'mypassword'});

//     // await repo.update('118e4a6c', { password: 'mypassword'});

    // await repo.create({ email: 'test@test.com '});

//     // await repo.delete('f1a23cbb');
//     // await repo.delete('9febe94b');

//     // const user = await repo.getOne('uiyuihkj');

//     // repo.create({ email: 'test@test.com', password: 'password'});

//     // const users = await repo.getAll();

//     // console.log(user);
// };

// test();

